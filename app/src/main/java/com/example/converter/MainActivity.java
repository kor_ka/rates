package com.example.converter;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;

import com.example.converter.matcher_adapter.DefaultDiffCallback;
import com.example.converter.matcher_adapter.MatcherAdapter;
import com.example.converter.model.ConverterModel;
import com.example.converter.model.Core;
import com.example.converter.view.CurrencyViewHolder;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;


public class MainActivity extends AppCompatActivity {
    private ConverterModel converterModel = Core.getInstance().getConverterModel();
    Disposable disposable;
    Disposable disposable2;
    MatcherAdapter<ConverterModel.ConvertedCurrency> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView list = findViewById(R.id.list);
        ProgressBar loader = findViewById(R.id.loader);
        list.setHasFixedSize(true);
        adapter = new MatcherAdapter<>();
        adapter.setDiffCallbackCallback((oldData, newData) -> new DefaultDiffCallback<ConverterModel.ConvertedCurrency>(oldData, newData){
            // don't need to update items via adapter, holders will care about it themselves
            // the reason is that item updates rendered by list will cause laggy move animations while items update
            @Override
            public boolean areContentsTheSame(int oldItemPosition, int newItemPosition) {
                return oldData.get(oldItemPosition).getId().equals(newData.get(newItemPosition).getId());
            }
        });
        adapter.getMatcher().add(c -> true, CurrencyViewHolder::new);
        disposable = converterModel.getConvertedCurrencies().observeOn(AndroidSchedulers.mainThread())
                .doOnNext(d -> {
                    if(d.size() > 0){
                        loader.setVisibility(View.GONE);
                    }
                })
                .subscribe(adapter::updateData);
        // scroll top on new item moved up
        disposable2 = converterModel.getCurrenciesOrder().observeOn(AndroidSchedulers.mainThread()).subscribe(o -> list.scrollToPosition(0));

        DefaultItemAnimator animator = new DefaultItemAnimator();
        animator.setSupportsChangeAnimations(false);
        list.setItemAnimator(animator);

        list.setAdapter(adapter);
        list.setLayoutManager(new LinearLayoutManager(list.getContext()));

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if(disposable != null){
            disposable.dispose();
        }
        if(disposable2 != null){
            disposable2.dispose();
        }
        adapter.dispose();
    }
}
