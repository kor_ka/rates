package com.example.converter.utils;
import com.example.converter.R;

public class FlagPicker {
    public  static int pickImage(String cur){
        switch (cur.toUpperCase()){
            case "USD":
                return R.drawable.ic_cur_us;
            case "EUR":
                return R.drawable.ic_cur_eu;
            case "SEK":
                return R.drawable.ic_cur_sweden;
            case "CAD":
                return R.drawable.ic_cur_canada;
            default:
                return R.drawable.ic_cur_fallback;
        }
    }
}
