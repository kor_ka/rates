package com.example.converter.network;

import java.io.IOException;

import okhttp3.OkHttpClient;
import okhttp3.Request;

public class OkHttpNetworkClient implements NetworkClient {
    private OkHttpClient client = new OkHttpClient();

    @Override
    public String get(String url) throws IOException {
        Request req = new Request.Builder().url(url).build();
        return client.newCall(req).execute().body().string();
    }
}
