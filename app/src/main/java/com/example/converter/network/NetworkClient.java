package com.example.converter.network;

import java.io.IOException;

public interface NetworkClient {
    String get(String url) throws IOException;
}
