package com.example.converter.view;

import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.example.converter.R;
import com.example.converter.matcher_adapter.ViewHolder;
import com.example.converter.model.ConverterModel;
import com.example.converter.model.Core;
import com.example.converter.utils.FlagPicker;

import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.util.Locale;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;

public class CurrencyViewHolder extends ViewHolder<ConverterModel.ConvertedCurrency> {
    private static NumberFormat format = new DecimalFormat("##.##");

    private ConverterModel converterModel = Core.getInstance().getConverterModel();
    private ConverterModel.ConvertedCurrency data;
    private Disposable disposable;

    private ImageView flagImage;
    private TextView currencyTitle;
    private TextView currencySubtitle;
    private EditText input;

    private boolean updating = false;
    private boolean hasFocus = false;


    public CurrencyViewHolder(@NonNull ViewGroup parent) {
        super(LayoutInflater.from(parent.getContext()).inflate(R.layout.converter_item, parent, false));
        flagImage = itemView.findViewById(R.id.flag_image);
        currencyTitle = itemView.findViewById(R.id.currency_title);
        currencySubtitle = itemView.findViewById(R.id.currency_subtitle);
        input = itemView.findViewById(R.id.input);
        itemView.setOnClickListener(v -> {
            if (data != null) {
                input.requestFocus();
                converterModel.bumpCurrency(data.getId());
            }
        });
        input.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (data != null && hasFocus && !updating) {
                    double val = 0d;
                    try {
                        val = Double.parseDouble(s.toString());
                    } catch (NumberFormatException e) {
                        e.printStackTrace();
                    }
                    converterModel.input(data.getId(), val);
                }
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        input.setOnFocusChangeListener((v, hasFocus) -> {
            this.hasFocus = hasFocus;
            if (hasFocus) {
                input.setSelection(input.length(), input.length());
                if (data != null) {
                    converterModel.input(data.getId(), data.getVal());
                }
            }
        });
    }

    @Override
    protected void onBind(@NonNull ConverterModel.ConvertedCurrency data) {
        onCurrencyUpdate(data);
        flagImage.setImageDrawable(itemView.getResources().getDrawable(FlagPicker.pickImage(data.getId())));
        currencyTitle.setText(data.getCurrency().getCurrencyCode());
        currencySubtitle.setText(data.getCurrency().getDisplayName(Locale.getDefault()));

        disposable = converterModel.getCurrencySubject(data.getId())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(this::onCurrencyUpdate);

    }

    private void onCurrencyUpdate(@NonNull ConverterModel.ConvertedCurrency data) {
        // don't touch input when it's focused - let user enter commas etc
        if (this.data != null && hasFocus) {
            return;
        }
        this.data = data;
        updating = true;
        input.setText(data.getVal() > 0 ? format.format(data.getVal()) : null);
        updating = false;
    }

    @Override
    protected void onUnbind() {
        if (disposable != null) {
            disposable.dispose();
        }
    }


}
