package com.example.converter.model;

import com.example.converter.network.NetworkClient;
import com.example.converter.storage.KeyValueStore;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;

public class RatesModel {
    private KeyValueStore store;
    private NetworkClient networkClient;
    private BehaviorSubject<LinkedHashMap<String, Double>> currencies = BehaviorSubject.createDefault(new LinkedHashMap<>());

    public RatesModel(KeyValueStore store, NetworkClient client) {
        super();
        this.store = store;
        this.networkClient = client;
    }


    public void start() {
        Observable<String> request = Observable
                .fromCallable(() -> networkClient.get("https://revolut.duckdns.org/latest?base=EUR"))
                .repeatWhen(r -> r.switchMap(r2 -> Observable.timer(1, TimeUnit.SECONDS)));

        String cached = store.getString("rates_cache", null);
        if(cached != null){
            request = request.startWith(cached);
        }

        request.map(s -> {
            LinkedHashMap<String, Double> res = parseBody(s);
            store.setString("rates_cache", s);
            return res;
        })
                .retryWhen(e -> e.switchMap(e2 -> Observable.timer(1, TimeUnit.SECONDS)))
                .subscribeOn(Schedulers.io())
                .subscribe(currencies);

    }

    LinkedHashMap<String, Double> parseBody(String s) throws JSONException {
        LinkedHashMap<String, Double> res = new LinkedHashMap<>();
        JSONObject jsonData = new JSONObject(s);
        JSONObject rates = jsonData.getJSONObject("rates");
        String rateKey;
        for (Iterator<String> iter = rates.keys(); iter.hasNext(); ) {
            rateKey = iter.next();
            res.put(rateKey, rates.getDouble(rateKey));
        }
        return res;
    }


    public Observable<LinkedHashMap<String, Double>> getRates() {
        return currencies;
    }
}
