package com.example.converter.model;

import com.example.converter.matcher_adapter.Diffable;
import com.example.converter.storage.KeyValueStore;
import com.example.converter.utils.Tuple;

import org.json.JSONArray;
import org.json.JSONException;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Currency;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import io.reactivex.Observable;
import io.reactivex.schedulers.Schedulers;
import io.reactivex.subjects.BehaviorSubject;


public class ConverterModel {
    private KeyValueStore store;

    private Observable<LinkedHashMap<String, Double>> ratesObservable;

    private BehaviorSubject<Tuple<String, Double>> currencyValueInput;
    private BehaviorSubject<ArrayList<String>> currenciesOrder;

    private BehaviorSubject<List<ConvertedCurrency>> convertedCurrencies;
    private Map<String, BehaviorSubject<ConvertedCurrency>> currencySubjectMap = new HashMap<>();


    ConverterModel(Observable<LinkedHashMap<String, Double>> ratesObservable, KeyValueStore store) {
        this.ratesObservable = ratesObservable;
        this.store = store;
        currencyValueInput = BehaviorSubject.createDefault(getCachedInput());
        currenciesOrder = BehaviorSubject.createDefault(getCachedOrder());
        convertedCurrencies = BehaviorSubject.createDefault(new ArrayList<>());

        // cache
        currenciesOrder.doOnNext(this::saveCachedOrder).subscribeOn(Schedulers.io()).subscribe();
        currencyValueInput.doOnNext(this::saveCachedInput).subscribeOn(Schedulers.io()).subscribe();
    }

    public void start() {
        Observable.combineLatest(
                ratesObservable,
                currencyValueInput,
                currenciesOrder,
                (rates, input, order) -> {
                    List<ConvertedCurrency> res = new ArrayList<>();
                    Double val = input.b;
                    Double rate = input.a.equals("EUR") ? 1d : rates.get(input.a);

                    if (rate == null || rates.size() == 0) {
                        return convertedCurrencies.getValue();
                    }
                    // not sure if it is correct trade off
                    // convert from selected to base currency is not very accurate, but i don't want to poll rates for each of them - to heavy :/
                    BigDecimal baseVal = new BigDecimal(val).divide(new BigDecimal(rate), 4, RoundingMode.HALF_EVEN);

                    ConvertedCurrency base = new ConvertedCurrency("EUR", baseVal.doubleValue());
                    getCurrencySubjectInner(base.getId()).onNext(base);
                    res.add(base);
                    for (Map.Entry<String, Double> r : rates.entrySet()) {
                        ConvertedCurrency convertedCurrency = new ConvertedCurrency(r.getKey(), new BigDecimal(r.getValue()).multiply(baseVal).doubleValue());
                        getCurrencySubjectInner(convertedCurrency.getId()).onNext(convertedCurrency);
                        res.add(convertedCurrency);
                    }
                    Collections.sort(res, (o1, o2) -> order.indexOf(o2.getId()) - order.indexOf(o1.getId()));
                    return res;
                })
                .subscribe(convertedCurrencies);
    }

    private synchronized BehaviorSubject<ConvertedCurrency> getCurrencySubjectInner(String id) {
        BehaviorSubject<ConvertedCurrency> res = currencySubjectMap.get(id);
        if (res == null) {
            res = BehaviorSubject.create();
            currencySubjectMap.put(id, res);
        }
        return res;
    }

    ////
    //  output
    ////
    public Observable<List<ConvertedCurrency>> getConvertedCurrencies() {
        return convertedCurrencies;
    }

    public BehaviorSubject<ArrayList<String>> getCurrenciesOrder() {
        return currenciesOrder;
    }

    public Observable<ConvertedCurrency> getCurrencySubject(String id) {
        return getCurrencySubjectInner(id);
    }

    ////
    //  input
    ////
    public void bumpCurrency(String id) {
        ArrayList<String> value = currenciesOrder.getValue();
        value.remove(id);
        value.add(id);
        currenciesOrder.onNext(new ArrayList<>(value));
    }

    public void input(String id, double value) {
        // anything can come from user
        value = Math.max(Math.min(value, Double.MAX_VALUE), Double.MIN_VALUE);
        currencyValueInput.onNext(new Tuple<>(id, value));
    }

    ////
    // cache
    ////
    private Tuple<String, Double> getCachedInput() {
        return new Tuple<>(store.getString("converter_input_currency", "EUR"), store.getDouble("converter_input_value", 1d));
    }

    private void saveCachedInput(Tuple<String, Double> input) {
        store.setString("converter_input_currency", input.a);
        store.setDouble("converter_input_value", input.b);
    }

    private ArrayList<String> getCachedOrder() {
        ArrayList<String> res = new ArrayList<>();
        String json = store.getString("converter_order", null);
        if (json != null) {
            try {
                JSONArray array = new JSONArray(json);
                for (int i = 0; i < array.length(); i++) {
                    res.add(array.getString(i));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return res;
    }

    private void saveCachedOrder(ArrayList<String> order) {
        JSONArray array = new JSONArray();
        for (String e : order) {
            array.put(e);
        }
        store.setString("converter_order", array.toString());
    }

    ////
    // entity
    ////
    public class ConvertedCurrency implements Diffable {
        private double val;
        private String id;
        private Currency currency;

        public ConvertedCurrency(String id, Double val) {
            this.id = id;
            this.val = val;
            this.currency = Currency.getInstance(id);
        }

        public double getVal() {
            return val;
        }

        public Currency getCurrency() {
            return currency;
        }

        @Override
        public String getId() {
            return id;
        }
    }
}


