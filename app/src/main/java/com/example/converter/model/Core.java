package com.example.converter.model;

import com.example.converter.network.NetworkClient;
import com.example.converter.storage.KeyValueStore;

public class Core {
    private static Core instance;

    private KeyValueStore store;
    private NetworkClient client;

    private RatesModel ratesModel;
    private ConverterModel converterModel;

    private Core(KeyValueStore store, NetworkClient client) {
        this.store = store;
        this.client = client;

        ratesModel = new RatesModel(store, client);
        converterModel = new ConverterModel(ratesModel.getRates(), store);

        ratesModel.start();
        converterModel.start();
    }

    public static void init(KeyValueStore store, NetworkClient client) {
        if (instance == null) {
            instance = new Core(store, client);
        }
    }

    public static Core getInstance() {
        return instance;
    }

    public ConverterModel getConverterModel() {
        return converterModel;
    }

}
