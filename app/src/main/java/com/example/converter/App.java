package com.example.converter;

import android.app.Application;
import android.content.Context;

import com.example.converter.model.Core;
import com.example.converter.network.OkHttpNetworkClient;
import com.example.converter.storage.SharedPreferencesStore;

public class App extends Application {
    static App app;
    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
        Core.init(
                new SharedPreferencesStore(getApplicationContext()),
                new OkHttpNetworkClient()
        );
    }

    public static Context getContext(){
        return app.getApplicationContext();
    }
}
