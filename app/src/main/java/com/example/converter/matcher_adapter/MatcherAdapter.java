package com.example.converter.matcher_adapter;

import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

// it's overkill for this task and i don't need any of this for just one viewholder
// but i just want cool abstraction for adapter, don't like default one
public class MatcherAdapter<T extends Diffable> extends RecyclerView.Adapter<ViewHolder<T>> {
    private List<T> data = new ArrayList<>();
    private Map<String, WeakReference<ViewHolder<T>>> holders = new HashMap<>();
    private Matcher<T> matcher= new Matcher<>();
    private DiffCallbackCallback<T> diffCallbackCallback;


    public void updateData(List<T> data){
        DiffUtil.Callback diffCallback = diffCallbackCallback != null ? diffCallbackCallback.onDiffNeeded(this.data, data) :  new DefaultDiffCallback<>(this.data, data);
        DiffUtil.DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);
        this.data = data;
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    // match and create view holders
    @Override
    public int getItemViewType(int position) {
        return matcher.getItemViewType(this.data.get(position));
    }


    @Override
    @NonNull
    public ViewHolder<T> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return matcher.onCreateViewHolder(parent, viewType);
    }

    // bind/unbind
    @Override
    public void onBindViewHolder(@NonNull ViewHolder<T> holder, int position) {
        T data = this.data.get(position);
        holders.put(data.getId(), new WeakReference<>(holder));
        holder.onBind(data);
    }

    @Override
    public void onViewRecycled(@NonNull ViewHolder<T> holder) {
        super.onViewRecycled(holder);
        holder.onUnbind();
    }

    public Matcher<T> getMatcher() {
        return matcher;
    }

    public void setDiffCallbackCallback(DiffCallbackCallback<T> diffCallbackCallback) {
        this.diffCallbackCallback = diffCallbackCallback;
    }

    public void dispose(){
        for(Map.Entry<String, WeakReference<ViewHolder<T>>> e: holders.entrySet()){
            ViewHolder<T> holder = e.getValue().get();
            if(holder != null){
                holder.onUnbind();
            }
        }
    }

    public interface DiffCallbackCallback<T>{
        DiffUtil.Callback onDiffNeeded(List<T> oldData, List<T> newData);
    }

}
