package com.example.converter.matcher_adapter;

import android.view.View;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public abstract class ViewHolder<T extends Diffable> extends RecyclerView.ViewHolder {
    protected View itemView;
    public ViewHolder(@NonNull View itemView) {
        super(itemView);
        this.itemView = itemView;
    }


    protected abstract void onBind(@NonNull T data);

    protected abstract void onUnbind();
}
