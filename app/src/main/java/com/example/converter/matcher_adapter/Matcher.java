package com.example.converter.matcher_adapter;


import android.view.ViewGroup;
import android.view.ViewParent;

import androidx.annotation.NonNull;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Map;

public class Matcher<T extends Diffable>{

    private LinkedHashMap<Filter<T>, Creator<T>> matchers = new LinkedHashMap<>();
    private ArrayList<Creator<T>> creators = new ArrayList<>();

    public Matcher add(Filter<T> filter, Creator<T> creator){
        matchers.put(filter, creator);
        creators.add(creator);
        return this;
    }

    public int getItemViewType(T data) {
        int i = 0;
        for(Map.Entry<Filter<T>, Creator<T>> e : matchers.entrySet()){
            if(e.getKey().match(data)){
                return i;
            }
            i++;
        }
        return -1;
    }


    public ViewHolder<T> onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if(viewType == -1){
            // TODO add unsupported view holder
        }
        return this.creators.get(viewType).create(parent);
    }

    public interface Creator<T extends Diffable>{
        ViewHolder<T> create(ViewGroup parent);
    }
    public interface Filter<T extends Diffable>{
        boolean match(T data);
    }

}


