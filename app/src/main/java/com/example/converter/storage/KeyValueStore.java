package com.example.converter.storage;

public interface KeyValueStore {
    void setString(String key, String value);
    String getString(String key, String defaultValue);

    void setDouble(String key, double value);
    double getDouble(String key, double defaultValue);

    void setInt(String key, int value);
    double getInt(String key, int defaultValue);
}
