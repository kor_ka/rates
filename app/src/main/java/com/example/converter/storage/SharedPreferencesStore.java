package com.example.converter.storage;

import android.content.Context;
import android.content.SharedPreferences;

import java.math.BigDecimal;

public class SharedPreferencesStore implements KeyValueStore {

    private Context context;
    private SharedPreferences preferences;

    public SharedPreferencesStore(Context context) {
        this.context = context;
        this.preferences = context.getSharedPreferences("global", Context.MODE_PRIVATE);
    }

    @Override
    public void setString(String key, String value) {
        preferences.edit().putString(key, value).apply();
    }

    @Override
    public String getString(String key, String defaultValue) {
        return preferences.getString(key, defaultValue);
    }

    @Override
    public void setDouble(String key, double value) {
        preferences.edit().putFloat(key, new BigDecimal(value).floatValue()).apply();
    }

    @Override
    public double getDouble(String key, double defaultValue) {
        return new BigDecimal(Math.min(preferences.getFloat(key, new BigDecimal(defaultValue).floatValue()), Double.MAX_VALUE)).doubleValue();
    }

    @Override
    public void setInt(String key, int value) {
        preferences.edit().putInt(key, value).apply();
    }

    @Override
    public double getInt(String key, int defaultValue) {
        return preferences.getInt(key, defaultValue);
    }
}
