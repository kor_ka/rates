package com.example.converter.dummy;

import com.example.converter.storage.KeyValueStore;

import java.util.Map;

public class DummyStore implements KeyValueStore {
    private Map<String, Object> data;
    public DummyStore(Map<String, Object> data) {
        this.data = data;
    }

    @Override
    public void setString(String key, String value) {
        data.put(key, value);
    }

    @Override
    public String getString(String key, String defaultValue) {
        Object val = data.get(key);
        return val != null ? (String) val : defaultValue;
    }

    @Override
    public void setDouble(String key, double value) {
        data.put(key, value);
    }

    @Override
    public double getDouble(String key, double defaultValue) {
        Object val = data.get(key);
        return val != null ? (double) val : defaultValue;
    }

    @Override
    public void setInt(String key, int value) {
        data.put(key, value);
    }

    @Override
    public double getInt(String key, int defaultValue) {
        Object val = data.get(key);
        return val != null ? (int) val : defaultValue;
    }
}
