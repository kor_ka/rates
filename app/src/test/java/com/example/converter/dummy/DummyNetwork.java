package com.example.converter.dummy;

import com.example.converter.network.NetworkClient;

import java.io.IOException;
import java.util.Map;

public class DummyNetwork implements NetworkClient {
    Map<String, String> data;

    public DummyNetwork(Map<String, String> data) {
        this.data = data;
    }

    @Override
    public String get(String url) {
        return data.get(url);
    }
}
