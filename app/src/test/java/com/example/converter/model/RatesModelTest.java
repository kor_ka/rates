package com.example.converter.model;

import com.example.converter.dummy.DummyNetwork;
import com.example.converter.dummy.DummyStore;
import com.example.converter.network.NetworkClient;
import com.example.converter.storage.KeyValueStore;

import static org.mockito.Mockito.*;

import org.json.JSONException;
import org.junit.Test;
import org.mockito.stubbing.Answer;

import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import static org.junit.Assert.*;


public class RatesModelTest {
    private static final String data = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":1.6146,\"BGN\":1.9536,\"BRL\":4.7863,\"CAD\":1.5321,\"CHF\":1.1262,\"CNY\":7.936,\"CZK\":25.686,\"DKK\":7.4482,\"GBP\":0.89722,\"HKD\":9.122,\"HRK\":7.4256,\"HUF\":326.12,\"IDR\":17304.0,\"ILS\":4.1658,\"INR\":83.622,\"ISK\":127.65,\"JPY\":129.4,\"KRW\":1303.3,\"MXN\":22.34,\"MYR\":4.8065,\"NOK\":9.7649,\"NZD\":1.7613,\"PHP\":62.521,\"PLN\":4.3134,\"RON\":4.6332,\"RUB\":79.484,\"SEK\":10.579,\"SGD\":1.5982,\"THB\":38.087,\"TRY\":7.6195,\"USD\":1.1621,\"ZAR\":17.803}}";
    private static final String cached = "{\"base\":\"EUR\",\"date\":\"2018-09-06\",\"rates\":{\"AUD\":2,\"BGN\":1.9536,\"BRL\":4.7863,\"CAD\":1.5321,\"CHF\":1.1262,\"CNY\":7.936,\"CZK\":25.686,\"DKK\":7.4482,\"GBP\":0.89722,\"HKD\":9.122,\"HRK\":7.4256,\"HUF\":326.12,\"IDR\":17304.0,\"ILS\":4.1658,\"INR\":83.622,\"ISK\":127.65,\"JPY\":129.4,\"KRW\":1303.3,\"MXN\":22.34,\"MYR\":4.8065,\"NOK\":9.7649,\"NZD\":1.7613,\"PHP\":62.521,\"PLN\":4.3134,\"RON\":4.6332,\"RUB\":79.484,\"SEK\":10.579,\"SGD\":1.5982,\"THB\":38.087,\"TRY\":7.6195,\"USD\":1.1621,\"ZAR\":17.803}}";

    @Test
    public void parseResponseTest() throws JSONException {
        RatesModel ratesModel = new RatesModel(new DummyStore(new HashMap<>()), new DummyNetwork(new HashMap<>()));
        LinkedHashMap<String, Double> parsed = ratesModel.parseBody(data);
        assertEquals(1.6146d, parsed.get("AUD"), 0);
    }

    @Test
    public void getAndParseResponseTest() throws IOException {

        NetworkClient client = mock(NetworkClient.class);
        when(client.get(any())).thenReturn(data);

        KeyValueStore store = new DummyStore(new HashMap<>());

        RatesModel ratesModel = new RatesModel(store, client);
        Iterator<LinkedHashMap<String, Double>> vals = ratesModel.getRates().skip(1).blockingIterable().iterator();
        ratesModel.start();

        LinkedHashMap<String, Double> cached = vals.next();
        assertEquals(1.6146d, cached.get("AUD"), 0);

    }

    @Test
    public void useCachedTest() throws IOException {

        NetworkClient client = mock(NetworkClient.class);
        when(client.get(any())).thenReturn(data);

        HashMap<String, Object> data = new HashMap<>();
        data.put("rates_cache", cached);
        KeyValueStore store = new DummyStore(data);

        RatesModel ratesModel = new RatesModel(store, client);
        Iterator<LinkedHashMap<String, Double>> vals = ratesModel.getRates().skip(1).blockingIterable().iterator();
        ratesModel.start();

        // cached
        LinkedHashMap<String, Double> cached = vals.next();
        assertEquals(2d, cached.get("AUD"), 0);

        // actual
        LinkedHashMap<String, Double> actual = vals.next();
        assertEquals(1.6146d, actual.get("AUD"), 0);

        // check cache updated
        RatesModel ratesModel2 = new RatesModel(store, client);
        Iterator<LinkedHashMap<String, Double>> vals2 = ratesModel.getRates().blockingIterable().iterator();
        ratesModel2.start();
        vals2.next();

        // cached
        LinkedHashMap<String, Double> cached2 = vals2.next();
        assertEquals(1.6146d, cached2.get("AUD"), 0);
    }

    @Test
    public void retryOnEverything() throws IOException {
        int[] failsCount = new int[]{0};
        NetworkClient client = mock(NetworkClient.class);
        when(client.get(any())).thenAnswer((Answer<String>) invocation -> {
            if(++failsCount[0] == 1){
                throw new IOException();
            }else if(failsCount[0] == 2){
                return "some invalid data";
            }else if(failsCount[0] == 3){
                return null;
            }
            return  data;
        });

        KeyValueStore store = new DummyStore(new HashMap<>());

        RatesModel ratesModel = new RatesModel(store, client);
        Iterator<LinkedHashMap<String, Double>> vals = ratesModel.getRates().skip(1).blockingIterable().iterator();
        ratesModel.start();
        LinkedHashMap<String, Double> res = vals.next();
        assertEquals(1.6146d, res.get("AUD"), 0);
    }

}