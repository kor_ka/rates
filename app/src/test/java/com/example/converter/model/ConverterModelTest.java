package com.example.converter.model;

import com.example.converter.dummy.DummyStore;
import com.example.converter.storage.KeyValueStore;

import org.junit.Test;

import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;

import io.reactivex.Observable;
import io.reactivex.subjects.BehaviorSubject;
import io.reactivex.subjects.PublishSubject;

import static org.junit.Assert.*;

public class ConverterModelTest {

    @Test
    public void convertCurrenciesTest() {
        KeyValueStore store = new DummyStore(new HashMap<>());
        PublishSubject<LinkedHashMap<String, Double>> ratesObservable = PublishSubject.create();

        ConverterModel converterModel = new ConverterModel(ratesObservable, store);
        Observable<List<ConverterModel.ConvertedCurrency>> convertedCurrencies = converterModel.getConvertedCurrencies();
        Iterator<List<ConverterModel.ConvertedCurrency>> allConverted = convertedCurrencies.skip(1).blockingIterable().iterator();
        Iterator<ConverterModel.ConvertedCurrency> usd = converterModel.getCurrencySubject("USD").blockingIterable().iterator();
        converterModel.start();

        LinkedHashMap<String, Double> data = new LinkedHashMap<>();
        data.put("USD", 2d);
        ratesObservable.onNext(data);

        // converted list
        List<ConverterModel.ConvertedCurrency> list = allConverted.next();
        assertEquals(2, list.size());
        assertEquals("EUR", list.get(0).getId());
        assertEquals(1, list.get(0).getVal(), 0);

        assertEquals("USD", list.get(1).getId());
        assertEquals(2, list.get(1).getVal(), 0);

        // single value
        ConverterModel.ConvertedCurrency convertedUsd = usd.next();
        assertEquals("USD", convertedUsd.getId());
        assertEquals(2, convertedUsd.getVal(), 0);

        // update rates
        LinkedHashMap<String, Double> data1 = new LinkedHashMap<>();
        data1.put("USD", 4d);
        ratesObservable.onNext(data1);

        list = allConverted.next();
        assertEquals("USD", list.get(1).getId());
        assertEquals(4, list.get(1).getVal(), 0);

        convertedUsd = usd.next();
        assertEquals(4, convertedUsd.getVal(), 0);

        // user input
        converterModel.input("USD", 10);

        list = allConverted.next();
        assertEquals(2.5, list.get(0).getVal(), 0);
        assertEquals(10, list.get(1).getVal(), 0);

        convertedUsd = usd.next();
        assertEquals(10, convertedUsd.getVal(), 0);

        // nothing changes on incorrect input
        converterModel.input("wopwop", 0);
        list = allConverted.next();
        assertEquals(2.5, list.get(0).getVal(), 0);
        assertEquals(10, list.get(1).getVal(), 0);

        // nothing breaks on big input
        converterModel.input("USD", Double.POSITIVE_INFINITY);
        allConverted.next();

        // nothing breaks on small input
        converterModel.input("USD", Double.NEGATIVE_INFINITY);
        allConverted.next();

        // input saved to cache
        converterModel.input("USD", 10);

        converterModel = new ConverterModel(ratesObservable, store);
        convertedCurrencies = converterModel.getConvertedCurrencies();
        allConverted = convertedCurrencies.skip(1).blockingIterable().iterator();
        converterModel.start();

        ratesObservable.onNext(data1);

        list = allConverted.next();
        assertEquals(10, list.get(1).getVal(), 0);
    }

    @Test
    public void bumpCurrencyTest() {
        KeyValueStore store = new DummyStore(new HashMap<>());
        PublishSubject<LinkedHashMap<String, Double>> ratesObservable = PublishSubject.create();

        ConverterModel converterModel = new ConverterModel(ratesObservable, store);
        Observable<List<ConverterModel.ConvertedCurrency>> convertedCurrencies = converterModel.getConvertedCurrencies();
        Iterator<List<ConverterModel.ConvertedCurrency>> allConverted = convertedCurrencies.skip(1).blockingIterable().iterator();
        converterModel.start();

        LinkedHashMap<String, Double> data = new LinkedHashMap<>();
        data.put("USD", 2d);
        ratesObservable.onNext(data);

        List<ConverterModel.ConvertedCurrency> list = allConverted.next();
        assertEquals("EUR", list.get(0).getId());
        assertEquals("USD", list.get(1).getId());

        // can change order
        converterModel.bumpCurrency("USD");
        list = allConverted.next();
        assertEquals("USD", list.get(0).getId());
        assertEquals("EUR", list.get(1).getId());

        // nothing changes on incorrect input
        converterModel.bumpCurrency("wopwop");
        list = allConverted.next();
        assertEquals("USD", list.get(0).getId());
        assertEquals("EUR", list.get(1).getId());

        // order saved to cache
        converterModel = new ConverterModel(ratesObservable, store);
        convertedCurrencies = converterModel.getConvertedCurrencies();
        allConverted = convertedCurrencies.skip(1).blockingIterable().iterator();
        converterModel.start();

        ratesObservable.onNext(data);

        list = allConverted.next();
        assertEquals("USD", list.get(0).getId());
        assertEquals("EUR", list.get(1).getId());
    }

}